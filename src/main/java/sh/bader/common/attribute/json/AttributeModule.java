package sh.bader.common.attribute.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.Module;
import sh.bader.common.attribute.Attribute;

public class AttributeModule extends Module {
    @Override
    public String getModuleName() {
        return "Attribute";
    }

    @Override
    public Version version() {
        return Version.unknownVersion();
    }

    @Override
    public void setupModule(SetupContext context) {
        // deserialization
        context.addDeserializers(new AttributeDeserializers());
        context.addTypeModifier(new AttributeTypeModifier());

        // serialization
        context.addSerializers(new AttributeSerializers());
        context.configOverride(Attribute.class)
                .setIncludeAsProperty(JsonInclude.Value.construct(JsonInclude.Include.NON_EMPTY, null));
    }
}
