package sh.bader.common.attribute;

import java.util.UUID;

import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;

import static org.hamcrest.Matchers.equalTo;

public class AttributesTest {
    @Test
    public void attribute() {
        // given
        Attribute attribute = Attribute.initialized("key", "value");

        // when
        String result = Attributes.toString(new Object(), attribute);

        // then
        MatcherAssert.assertThat(result, equalTo("Object[key=value]"));
    }

    @Test
    public void attributeWithIDEntity() {
        // given
        DummyEntity entity = new DummyEntity(UUID.randomUUID());
        DummyID id = entity.getId();

        Attribute attribute = Attribute.initialized("key", entity);

        // when
        String result = Attributes.toString(new Object(), attribute);

        // then
        MatcherAssert.assertThat(result, equalTo("Object[key="+id.toString()+"]"));
    }
}
