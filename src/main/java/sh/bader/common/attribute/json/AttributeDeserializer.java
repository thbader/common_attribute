package sh.bader.common.attribute.json;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.ValueInstantiator;
import com.fasterxml.jackson.databind.deser.std.ReferenceTypeDeserializer;
import com.fasterxml.jackson.databind.jsontype.TypeDeserializer;
import sh.bader.common.attribute.Attribute;

final class AttributeDeserializer extends ReferenceTypeDeserializer<Attribute<?>> {
    private String name;

    public AttributeDeserializer(
            JavaType fullType,
            ValueInstantiator inst,
            TypeDeserializer typeDeser,
            JsonDeserializer<?> deser
    ) {
        super(fullType, inst, typeDeser, deser);
    }

    @Override
    public Attribute<?> deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        this.name = p.getCurrentName();

        return super.deserialize(p, ctxt);
    }

    @Override
    public Object deserializeWithType(JsonParser p, DeserializationContext ctxt, TypeDeserializer typeDeserializer) throws IOException {
        this.name = p.getCurrentName();

        return super.deserializeWithType(p, ctxt, typeDeserializer);
    }

    @Override
    public Object deserializeWithType(JsonParser p, DeserializationContext ctxt, TypeDeserializer typeDeserializer, Attribute<?> intoValue) throws IOException {
        this.name = p.getCurrentName();

        return super.deserializeWithType(p, ctxt, typeDeserializer, intoValue);
    }

    @Override
    protected ReferenceTypeDeserializer<Attribute<?>> withResolved(TypeDeserializer typeDeser, JsonDeserializer<?> valueDeser) {
        return new AttributeDeserializer(this._fullType, this._valueInstantiator, typeDeser, valueDeser);
    }

    @Override
    public Attribute<?> getNullValue(DeserializationContext ctxt) {
        return Attribute.uninitialized(this.getName(ctxt)).setValue((String) null);
    }

    @Override
    public Attribute<?> referenceValue(Object contents) {
        return Attribute.uninitialized(this.name).setValue(contents);
    }

    @Override
    public Attribute<?> updateReference(Attribute<?> reference, Object contents) {
        return null; //reference.setValue((Object) contents);
    }

    @Override
    public Object getReferenced(Attribute<?> reference) {
        return reference.getValue().orElse(null);
    }

    private String getName(DeserializationContext ctxt) {
        try {
            return ctxt.getParser().getCurrentName();
        } catch (IOException e) {
            return null;
        }
    }
}
