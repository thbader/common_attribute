package sh.bader.common.attribute;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Wrapper for value to support the additional state uninitialized.
 *
 * @param <E> the attribute type.
 */
public final class Attribute<E> {
    private final static Logger LOG = LogManager.getLogger();

    private final String name;

    private String path;

    private E value = null;

    private boolean initialized = false;

    private final boolean secured;

    /**
     * Constructs an uninitialized attribute.
     *
     * @param name the name of the attribute.
     * @param secured {@code true}, if the value is secured (e.g. passwords), {@code false} otherwise.
     */
    private Attribute(final String name, final boolean secured) {
        this.name = name;
        this.path = name;
        this.secured = secured;
    }

    public Attribute<E> copy() {
        Attribute<E> copy = new Attribute<>(name, secured);
        copy.path = this.path;
        copy.value = this.value;
        copy.initialized = this.initialized;

        return copy;
    }

    public Attribute<E> uninitializedCopy() {
        Attribute<E> copy = Attribute.uninitialized(name, secured);
        copy.path = this.path;

        return copy;
    }

    /**
     * Create an uninitialized attribute.
     *
     * @param name the name.
     * @param <E>  the attribute type.
     * @return the uninitialized attribute.
     */
    public static <E> Attribute<E> uninitialized(final String name) {
        return uninitialized(name, false);
    }

    /**
     * Create an uninitialized attribute.
     *
     * @param name the name.
     * @param secured {@code true}, if the value is secured (e.g. passwords), {@code false} otherwise.
     * @param <E>  the attribute type.
     * @return the uninitialized attribute.
     */
    public static <E> Attribute<E> uninitialized(final String name, boolean secured) {
        return new Attribute<>(name, secured);
    }

    /**
     * Create an initialized attribute.
     *
     * @param name  the name.
     * @param value the value.
     * @param <E>   the attribute type.
     * @return the uninitialized attribute.
     */
    public static <E> Attribute<E> initialized(final String name, final E value) {
        return initialized(name, false, value);
    }

    /**
     * Create an initialized attribute.
     *
     * @param name  the name.
     * @param secured {@code true}, if the value is secured (e.g. passwords), {@code false} otherwise.
     * @param value the value.
     * @param <E>   the attribute type.
     * @return the uninitialized attribute.
     */
    public static <E> Attribute<E> initialized(final String name, boolean secured, final E value) {
        return new Attribute<E>(name, secured).setValue(value);
    }

    /**
     * Get the name.
     *
     * @return the name.
     */
    public String getName() {
        return this.name;
    }

    public boolean isSecured() {
        return secured;
    }

    /**
     * Get the path of the attribute.
     * The path is equal to the name, except for embedded attributes containing the parent names.
     *
     * @return the path of the attribute.
     */
    public String getPath() {
        return this.path;
    }

    /**
     * Get the value.
     *
     * @return the value.
     */
    public Optional<E> getValue() {
        return Optional.ofNullable(this.value);
    }

    /**
     * Set the value.
     * <p>
     * <i>The attribute will be marked as initialized.</i>
     * </p>
     *
     * @param value the value.
     * @return the attribute itself.
     */
    public Attribute<E> setValue(final E value) {
        this.value = Attribute.trim(value);
        this.initialized = true;


        Attribute.rewritePath(this);

        return this;
    }

    public Attribute<E> setValue(final Supplier<E> supplier) {
        return setValue(supplier.get());
    }

    public Attribute<E> copy(final Attribute<E> attribute) {
        if (attribute.isInitialized()) {
            this.setValue(attribute.getValue().orElse(null));
        } else {
            this.clear();
        }

        return this;
    }

    /**
     * Check if the attribute is initialized.
     *
     * @return {@code true}, if the attribute is initialized, {@code false} otherwise.
     */
    public boolean isInitialized() {
        return this.initialized;
    }

    /**
     * Clear the value and mark the attribute as uninitialized.
     */
    public void clear() {
        this.value = null;
        this.initialized = false;
    }

    @Override
    public String toString() {
        if (this.initialized) {
            if (this.value == null) {
                return "<null>";
            }

            if (this.secured) {
                return "<secured>";
            }

            return String.valueOf(this.value);
        }

        return "<uninitialized>";
    }

    /**
     * Calls the consumer if the attribute is initialized.
     *
     * @param consumer the consumer.
     */
    public void ifInitialized(final Consumer<E> consumer) {
        if (this.isInitialized()) {
            consumer.accept(this.getValue().orElse(null));
        }
    }

    /**
     * Calls the consumer if the attribute is not initialized.
     *
     * @param supplier the supplier.
     */
    public void setValueIfNotInitialized(final Supplier<E> supplier) {
        if (!this.isInitialized()) {
            this.setValue(supplier.get());
        }
    }

    /**
     * Trim value, if the input is a string.
     *
     * @param value the value.
     * @return the trimmed value.
     */
    private static <E> E trim(final E value) {
        if (value instanceof String) {
            return (E) StringUtils.trimToNull((String) value);
        } else {
            return value;
        }
    }

    private static void rewritePath(final Attribute<?> attribute) {
        if (attribute.value != null) {
            getFields(attribute.value.getClass()).stream()
                    .filter(field -> Attribute.class.equals(field.getType()))
                    .forEach(field -> {
                        final boolean accessible = field.canAccess(attribute.value);
                        try {
                            if (!accessible) {
                                field.setAccessible(true);
                            }
                            final Attribute<?> child = (Attribute<?>) field.get(attribute.value);
                            Attribute.updatePath(attribute, child);
                            Attribute.rewritePath(child);
                        } catch (final IllegalAccessException e) {
                            LOG.warn("faild to update attribute path: {}", field.getName());
                        } finally {
                            if (!accessible) {
                                field.setAccessible(false);
                            }
                        }
                    });
        }
    }

    private static List<Field> getFields(Class<?> clazz) {
        ArrayList<Field> result = new ArrayList<>();
        if (clazz.getSuperclass() != null) {
            result.addAll(getFields(clazz.getSuperclass()));
        }
        result.addAll((Arrays.asList(clazz.getDeclaredFields())));


        return result;
    }

    private static void updatePath(final Attribute<?> parent, final Attribute<?> child) {
        child.path = parent.path + "." + child.name;
    }
}
