package sh.bader.common.attribute.json.example;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import sh.bader.common.attribute.Attribute;

public class BooleanExample {
    private final Attribute<Boolean> name = Attribute.uninitialized("name");

    public Attribute<Boolean> getName() {
        return this.name;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("name", this.name)
                .toString();
    }
}
