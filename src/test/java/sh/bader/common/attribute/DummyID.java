package sh.bader.common.attribute;

import java.util.UUID;

import sh.bader.common.id.ID;

public class DummyID implements ID<DummyID, DummyEntity> {
    private UUID id;

    public DummyID(UUID id)
    {
        this.id = id;
    }

    @Override
    public Class<DummyEntity> getEntityClass() {
        return DummyEntity.class;
    }

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public int compareTo(DummyID o) {
        return this.id.compareTo(o.getId());
    }
}
