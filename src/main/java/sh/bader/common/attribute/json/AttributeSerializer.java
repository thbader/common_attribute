package sh.bader.common.attribute.json;

import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;
import com.fasterxml.jackson.databind.ser.std.ReferenceTypeSerializer;
import com.fasterxml.jackson.databind.type.ReferenceType;
import com.fasterxml.jackson.databind.util.NameTransformer;
import sh.bader.common.attribute.Attribute;

public class AttributeSerializer extends ReferenceTypeSerializer<Attribute<?>> {

    public AttributeSerializer(ReferenceType fullType, boolean staticTyping, TypeSerializer vts, JsonSerializer<Object> ser) {
        super(fullType, staticTyping, vts, ser);
    }

    protected AttributeSerializer(AttributeSerializer base, BeanProperty property, TypeSerializer vts, JsonSerializer<?> valueSer, NameTransformer unwrapper, Object suppressableValue, boolean suppressNulls) {
        super(base, property, vts, valueSer, unwrapper, suppressableValue, suppressNulls);
    }

    @Override
    protected ReferenceTypeSerializer<Attribute<?>> withResolved(BeanProperty prop, TypeSerializer vts, JsonSerializer<?> valueSer, NameTransformer unwrapper) {
        return new AttributeSerializer(this, prop, vts, valueSer, unwrapper, this._suppressableValue, this._suppressNulls);
    }

    @Override
    public ReferenceTypeSerializer<Attribute<?>> withContentInclusion(Object suppressableValue, boolean suppressNulls) {
        return new AttributeSerializer(this, this._property, this._valueTypeSerializer, this._valueSerializer, this._unwrapper, suppressableValue, suppressNulls);
    }

    @Override
    public boolean isEmpty(SerializerProvider provider, Attribute<?> value) {
        return value == null || !value.isInitialized();
    }

    @Override
    protected boolean _isValuePresent(Attribute<?> value) {
        return value.getValue().isPresent();
    }

    @Override
    protected Object _getReferenced(Attribute<?> value) {
        return value.getValue().orElse(null);
    }

    @Override
    protected Object _getReferencedIfPresent(Attribute<?> value) {
        return value.getValue().orElse(null);
    }
}
