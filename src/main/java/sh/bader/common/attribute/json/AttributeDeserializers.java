package sh.bader.common.attribute.json;

import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.Deserializers;
import com.fasterxml.jackson.databind.jsontype.TypeDeserializer;
import com.fasterxml.jackson.databind.type.ReferenceType;
import sh.bader.common.attribute.Attribute;

final class AttributeDeserializers extends Deserializers.Base {
    @Override
    public JsonDeserializer<?> findReferenceDeserializer(
        ReferenceType refType,
        DeserializationConfig config,
        BeanDescription beanDesc,
        TypeDeserializer contentTypeDeserializer,
        JsonDeserializer<?> contentDeserializer
    ) {
        if (refType.hasRawClass(Attribute.class)) {
            return new AttributeDeserializer(refType, null, contentTypeDeserializer, contentDeserializer);
        }

        return null;
    }
}
