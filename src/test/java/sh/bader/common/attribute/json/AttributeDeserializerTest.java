package sh.bader.common.attribute.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import sh.bader.common.attribute.json.example.BooleanExample;
import sh.bader.common.attribute.json.example.IntegerExample;
import sh.bader.common.attribute.json.example.StringExample;

import static com.github.npathai.hamcrestopt.OptionalMatchers.isEmpty;
import static com.github.npathai.hamcrestopt.OptionalMatchers.isPresentAnd;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class AttributeDeserializerTest {

    private ObjectMapper objectMapper;

    @BeforeEach
    public void before() {
        this.objectMapper = new ObjectMapper()
                .registerModule(new AttributeModule());
    }

    @Test
    public void deserializeWithStringValue() throws Exception {
        // given
        String json = "{\"name\": \"abc\"}";

        // when
        StringExample result = this.objectMapper.readValue(json, StringExample.class);

        // then
        assertThat(result, hasProperty("name", allOf(
                hasProperty("name", equalTo("name")),
                hasProperty("value", isPresentAnd(equalTo("abc"))),
                hasProperty("initialized", equalTo(true))
        )));
    }

    @Test
    public void deserializeWithBooleanValue() throws Exception {
        // given
        String json = "{\"name\": true}";

        // when
        BooleanExample result = this.objectMapper.readValue(json, BooleanExample.class);

        // then
        assertThat(result, hasProperty("name", allOf(
                hasProperty("name", equalTo("name")),
                hasProperty("value", isPresentAnd(equalTo(true))),
                hasProperty("initialized", equalTo(true))
        )));
    }

    @Test
    public void deserializeWithInvalidBooleanValue() {
        // given
        String json = "{\"name\": \"test\"}";

        // when
        assertThrows(InvalidFormatException.class, () -> this.objectMapper.readValue(json, BooleanExample.class));

        // then
    }

    @Test
    public void deserializeWithIntegerValue() throws Exception {
        // given
        String json = "{\"name\": 5}";

        // when
        IntegerExample result = this.objectMapper.readValue(json, IntegerExample.class);

        // then
        assertThat(result, hasProperty("name", allOf(
                hasProperty("name", equalTo("name")),
                hasProperty("value", isPresentAnd(equalTo(5))),
                hasProperty("initialized", equalTo(true))
        )));
    }

    @Test
    public void deserializeWithNull() throws Exception {
        // given
        String json = "{\"name\": null}";

        // when
        StringExample result = this.objectMapper.readValue(json, StringExample.class);

        // then
        assertThat(result, hasProperty("name", allOf(
                hasProperty("name", equalTo("name")),
                hasProperty("value", isEmpty()),
                hasProperty("initialized", equalTo(true))
        )));
    }

    @Test
    public void deserializeWhenMissing() throws Exception {
        // given
        String json = "{}";

        // when
        StringExample result = this.objectMapper.readValue(json, StringExample.class);

        // then
        assertThat(result, hasProperty("name", allOf(
                hasProperty("name", equalTo("name")),
                hasProperty("value", isEmpty()),
                hasProperty("initialized", equalTo(false))
        )));
    }
}
