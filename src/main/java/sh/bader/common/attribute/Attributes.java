package sh.bader.common.attribute;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import sh.bader.common.id.IDEntity;

public final class Attributes {
    private Attributes() {
    }

    public static String toString(Object attributeContainer, Attribute<?>... attributes) {
        ToStringBuilder builder = new ToStringBuilder(attributeContainer, ToStringStyle.SHORT_PREFIX_STYLE);
        for (Attribute<?> attribute : attributes) {
            if (attribute.isInitialized() && attribute.getValue().isPresent()) {
                builder.append(attribute.getName(), toString(attribute));
            }
        }

        return builder.toString();
    }

    private static String toString(Attribute<?> attribute) {
        Object value = attribute.getValue().orElse(null);
        if (!attribute.isSecured() && value instanceof IDEntity) {
            return ((IDEntity<?, ?>) value).getId().toString();
        }

        return attribute.toString();
    }
}
