package sh.bader.common.attribute.json.example;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import sh.bader.common.attribute.Attribute;

public class IntegerExample {
    private final Attribute<Integer> name = Attribute.uninitialized("name");

    public Attribute<Integer> getName() {
        return this.name;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("name", this.name)
                .toString();
    }
}
