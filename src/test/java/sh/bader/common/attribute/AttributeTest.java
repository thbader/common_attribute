package sh.bader.common.attribute;

import java.util.ArrayList;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.github.npathai.hamcrestopt.OptionalMatchers.isEmpty;
import static com.github.npathai.hamcrestopt.OptionalMatchers.isPresentAnd;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.jupiter.api.Assertions.fail;

public class AttributeTest {
    private static final String NAME = "any-name";
    private static final String NAME_2 = "any-name-2";

    private static final String VALUE = "any-value";

    private Attribute<String> attribute;

    @BeforeEach
    public void before() {
        this.attribute = Attribute.uninitialized(NAME);
    }

    @Test
    public void uninitialized() {
        // given

        // when

        // then
        assertThat(this.attribute, allOf(
                hasProperty("name", equalTo(NAME)),
                hasProperty("value", isEmpty()),
                hasProperty("initialized", equalTo(false))
        ));
    }

    @Test
    public void setValueWithString() {
        // given

        // when
        this.attribute.setValue(VALUE + " ");

        //  then
        assertThat(this.attribute, allOf(
                hasProperty("name", equalTo(NAME)),
                hasProperty("value", isPresentAnd(equalTo(VALUE))),
                hasProperty("initialized", equalTo(true))
        ));
    }

    @Test
    public void setValueWithInteger() {
        // given
        final Attribute<Integer> attribute = Attribute.uninitialized(NAME);

        // when
        final Integer VALUE = 5;
        attribute.setValue(VALUE);

        //  then
        assertThat(attribute, allOf(
                hasProperty("name", equalTo(NAME)),
                hasProperty("value", isPresentAnd(equalTo(VALUE))),
                hasProperty("initialized", equalTo(true))
        ));
    }

    @Test
    public void copyWithAttributeInitialized() {
        // given
        this.attribute.setValue(VALUE);
        assertThat(this.attribute, allOf(
                hasProperty("name", equalTo(NAME)),
                hasProperty("value", isPresentAnd(equalTo(VALUE))),
                hasProperty("initialized", equalTo(true))
        ));

        // when
        final String VALUE_2 = "any-value-2";
        final Attribute<String> copyAttribute = Attribute.initialized(NAME_2, VALUE_2);

        this.attribute.copy(copyAttribute);

        // then
        assertThat(this.attribute, allOf(
                hasProperty("name", equalTo(NAME)),
                hasProperty("value", isPresentAnd(equalTo(VALUE_2))),
                hasProperty("initialized", equalTo(true))
        ));
    }

    @Test
    public void copyWithAttributeUninitialized() {
        // given
        this.attribute.setValue(VALUE);
        assertThat(this.attribute, allOf(
                hasProperty("name", equalTo(NAME)),
                hasProperty("value", isPresentAnd(equalTo(VALUE))),
                hasProperty("initialized", equalTo(true))
        ));

        // when
        final Attribute<String> copyAttribute = Attribute.uninitialized(NAME_2);

        this.attribute.copy(copyAttribute);

        // then
        assertThat(this.attribute, allOf(
                hasProperty("name", equalTo(NAME)),
                hasProperty("value", isEmpty()),
                hasProperty("initialized", equalTo(false))
        ));
    }

    @Test
    public void ifInitializedTrue() {
        // given

        // when
        this.attribute.ifInitialized(value -> fail());

        // then
    }

    @Test
    public void ifInitializedFalse() {
        // given
        final ArrayList<String> check = new ArrayList<>();
        this.attribute.setValue(VALUE);

        // when
        this.attribute.ifInitialized(check::add);

        // then
        assertThat(check, contains(VALUE));
    }

    @Test
    public void checkRecursiveNames() {
        final DeepSecond second = new DeepSecond();
        second.getValue1().setValue("def");
        second.getValue2().setValue((String) null);

        final First first = new First();
        first.getValue1().setValue("abc");
        first.getValue2().setValue(second);
        first.getValue3().setValue((Second) null);

        final Recursive recursive = new Recursive();
        recursive.getFirst().setValue(first);

        assertThat(recursive, hasProperty("first", allOf(
                hasProperty("name", equalTo("first")),
                hasProperty("path", equalTo("first")),
                hasProperty("value", isPresentAnd(allOf(
                        hasProperty("value1", allOf(
                                hasProperty("name", equalTo("value1")),
                                hasProperty("path", equalTo("first.value1")),
                                hasProperty("value", isPresentAnd(equalTo("abc"))),
                                hasProperty("initialized", equalTo(true))
                        )),
                        hasProperty("value2", allOf(
                                hasProperty("name", equalTo("value2")),
                                hasProperty("path", equalTo("first.value2")),
                                hasProperty("value", isPresentAnd(allOf(
                                        hasProperty("value1", allOf(
                                                hasProperty("name", equalTo("value1")),
                                                hasProperty("path", equalTo("first.value2.value1")),
                                                hasProperty("value", isPresentAnd(equalTo("def"))),
                                                hasProperty("initialized", equalTo(true))
                                        )),
                                        hasProperty("value2", allOf(
                                                hasProperty("name", equalTo("value2")),
                                                hasProperty("path", equalTo("first.value2.value2")),
                                                hasProperty("value", isEmpty()),
                                                hasProperty("initialized", equalTo(true))
                                        ))
                                ))),
                                hasProperty("initialized", equalTo(true))
                        )),
                        hasProperty("value3", allOf(
                                hasProperty("name", equalTo("value3")),
                                hasProperty("path", equalTo("first.value3")),
                                hasProperty("value", isEmpty()),
                                hasProperty("initialized", equalTo(true))
                        ))
                ))),
                hasProperty("initialized", equalTo(true))
        )));
    }

    public static class Recursive {
        private final Attribute<First> first = Attribute.uninitialized("first");

        public Attribute<First> getFirst() {
            return this.first;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                    .append("first", this.first)
                    .toString();
        }
    }

    public static class First {
        private final Attribute<String> value1 = Attribute.uninitialized("value1");

        private final Attribute<Second> value2 = Attribute.uninitialized("value2");

        private final Attribute<Second> value3 = Attribute.uninitialized("value3");

        public Attribute<String> getValue1() {
            return this.value1;
        }

        public Attribute<Second> getValue2() {
            return this.value2;
        }

        public Attribute<Second> getValue3() {
            return this.value3;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                    .append("value1", this.value1)
                    .append("value2", this.value2)
                    .append("value3", this.value3)
                    .toString();
        }
    }

    public static class DeepSecond extends Second {
    }

    public static class Second {
        private final Attribute<String> value1 = Attribute.uninitialized("value1");
        private final Attribute<String> value2 = Attribute.uninitialized("value2");

        public Attribute<String> getValue1() {
            return this.value1;
        }

        public Attribute<String> getValue2() {
            return this.value2;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                    .append("value1", this.value1)
                    .append("value2", this.value2)
                    .toString();
        }
    }
}
