package sh.bader.common.attribute.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import sh.bader.common.attribute.json.example.BooleanExample;
import sh.bader.common.attribute.json.example.IntegerExample;
import sh.bader.common.attribute.json.example.StringExample;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class AttributeSerializerTest {

    private ObjectMapper objectMapper;

    @BeforeEach
    public void before() {
        this.objectMapper = new ObjectMapper()
                .registerModule(new AttributeModule());
    }

    @Test
    public void serializeWithStringValue() throws Exception {
        // given
        StringExample example = new StringExample();
        example.getName().setValue("Spiderman");

        // when
        String json = this.objectMapper.writeValueAsString(example);

        // then
        assertThat(json, equalTo("{\"name\":\"Spiderman\"}"));
    }

    @Test
    public void serializeWithIntegerValue() throws Exception {
        // given
        IntegerExample example = new IntegerExample();
        example.getName().setValue(5);

        // when
        String json = this.objectMapper.writeValueAsString(example);

        // then
        assertThat(json, equalTo("{\"name\":5}"));
    }

    @Test
    public void serializeWithBooleanValue() throws Exception {
        // given
        BooleanExample example = new BooleanExample();
        example.getName().setValue(true);

        // when
        String json = this.objectMapper.writeValueAsString(example);

        // then
        assertThat(json, equalTo("{\"name\":true}"));
    }

    @Test
    public void serializeWithNullValue() throws Exception {
        // given
        StringExample example = new StringExample();
        example.getName().setValue((String) null);

        // when
        String json = this.objectMapper.writeValueAsString(example);

        // then
        assertThat(json, equalTo("{\"name\":null}"));
    }

    @Test
    public void serializeWithUninitializedValue() throws Exception {
        // given
        StringExample example = new StringExample();

        // when
        String json = this.objectMapper.writeValueAsString(example);

        // then
        assertThat(json, equalTo("{}"));
    }
}
